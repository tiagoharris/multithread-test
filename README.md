# multithread-test

This is the source to ilustrate how to test multithreaded code with JUnit.

## Stack:
* [Spring Boot](https://spring.io/projects/spring-boot)
* [H2 Database](http://www.h2database.com/html/main.html)
* [Maven](https://maven.apache.org/)


