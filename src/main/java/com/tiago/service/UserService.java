package com.tiago.service;

import com.tiago.entity.User;

/**
 * Service to manage users.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
public interface UserService {

  /**
   * Saves a user
   * 
   * @param user to be saved
   * @return the saved user
   */
  User save(User user);
  
}
