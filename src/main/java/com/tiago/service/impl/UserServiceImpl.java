package com.tiago.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tiago.entity.User;
import com.tiago.repository.UserRepository;
import com.tiago.service.UserService;

/**
 * Implements {@link UserService} interface.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Service
public class UserServiceImpl implements UserService {

  @Autowired
  UserRepository userRepository;

  /* (non-Javadoc)
   * @see com.tiago.service.UserService#save(com.tiago.entity.User)
   */
  @Override
  public synchronized User save(User user) {
    userRepository.save(user);

    return user;
  }
}
