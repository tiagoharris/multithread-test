package com.tiago.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.tiago.entity.User;

/**
 * Repository for {@link User} entity.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
}
