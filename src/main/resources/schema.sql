CREATE TABLE IF NOT EXISTS `user` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  
  PRIMARY KEY(`id`),
  UNIQUE(`name`, `email`)
  
) engine=InnoDB default charset=utf8;