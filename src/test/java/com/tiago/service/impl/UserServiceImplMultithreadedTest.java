package com.tiago.service.impl;

import static org.junit.Assert.assertEquals;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tiago.entity.User;
import com.tiago.repository.UserRepository;
import com.tiago.service.UserService;

/**
 * This class represents a test case of multiple threads attempting to 
 * save the same User.
 * 
 * @author Tiago Melo (tiagoharris@gmail.com)
 *
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserServiceImplMultithreadedTest {

  @Autowired
  UserService service;

  @Autowired
  UserRepository userRepository;

  AtomicInteger threadExecutionCount = new AtomicInteger();

  @Test
  public void testSaveMethod() throws InterruptedException, ExecutionException {
    // Number of threads that will try to persist the same User object
    int threadCount = 10;

    // The User object
    User newUser = buildUser();

    // A task that persists the User object
    Callable<User> task = getCallable(newUser);

    // A list of the task mentioned above
    List<Callable<User>> tasks = Collections.nCopies(threadCount, task);

    // The thread pool that will execute all tasks from the list
    ExecutorService executorService = Executors.newFixedThreadPool(threadCount);

    // Here we are asking to execute all the tasks
    List<Future<User>> futures = executorService.invokeAll(tasks);

    // This set will hold the persisted User object
    HashSet<User> userSet = new HashSet<User>();
    
    // Here we will check for exceptions
    for (Future<User> future : futures) {
      // Counts the number of threads that were executed
      threadExecutionCount.incrementAndGet();

      // future.get() will throw java.util.concurrent.ExecutionException if an exception was
      // thrown by any task.
      //
      // If UserServiceImpl.save(User user) method were not
      // synchronized, a task would throw a org.springframework.dao.DataIntegrityViolationException.
      //
      // The User is stored only once at the database; future.get() returns the
      // same object.
      userSet.add(future.get());
    }

    // Since a Set does not stores duplicated objects, this set will contain only
    // one User object.
    assertEquals(1, userSet.size());

    // Here we assure that all threads were executed
    assertEquals(threadExecutionCount.get(), threadCount);

    // There will be only one User object in the database
    assertEquals(newUser, userSet.iterator().next());
  }
  
  private Callable<User> getCallable(User newUser) {
    Callable<User> task = new Callable<User>() {
      @Override
      public User call() {
        return service.save(newUser);
      }
    };

    return task;
  }

  private User buildUser() {
    User newUser = new User();
    newUser.setName("Steve Harris");
    newUser.setEmail("steve@ironmaiden.com");
    
    return newUser;
  }
}
